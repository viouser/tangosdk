import json
import argparse
import sys
import setup

response = None
admin_email = '{ "email": xiaog@vmware.com }'
member_email = '{ "email": hmourad@vmware.com }'
zoneid = '{ zoneId: %s }'
  
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        __file__,
        description="Simple Tango Consumption Examples"
    )
    # class option
    parser.add_argument("-r", "--resource",
        help="specify type of resource - cloud accout, flavor, image, blueprint, network compute",
        type=str,
        default="flavor"
    )
    # rest option
    parser.add_argument("-o", "--option",
        help="rest command - get put translate delete",
        type=str,
        default="get"
    )
    # name
    parser.add_argument("-n", "--name",
        help="assign a name to the resourcee",
        type=str,
        default=""
    )
    # multiple values allowed for this option

    parser.add_argument("-id", "--id",
        help="use blueprint get to retrive the blueprint id",
        type=str,
        default="7516924fc4e7557243400f2d28"
    )
    parser.add_argument("-l", "--location",
        help="use blueprint get to retrive the blueprint id",
        type=str,
        default="7516924fc4e7557243400f2d28"
    )

    # multiple values allowed for this option

    parser.add_argument("-d", "--description",
        help="description field",
        type=str,
        default="API generated resource"
    )

    args = parser.parse_args()
    print("option {}".format(args.option))
    print("resource {}".format(args.resource))

if (args.resource == 'flavor'):
    flavor_profile = setup.FlavorProfile(setup.SERVER, setup.API_KEY)
    if (args.option == 'get'):
        response=flavor_profile.get_flavor_profiles()
    elif (args.option == 'post'):
        my_flavor_map = flavor_profile.create_flavor_mapping(small="t2.small", medium="t2.medium", large="t2.large")
        # id here denotes region id
        response = flavor_profile.post_flavor_profile('test', my_flavor_map, args.id)
    else:
        response = "INVALID options only get, post and delete are supported image options"
elif (args.resource == 'image'):
    image_profile = setup.ImageProfile(setup.SERVER, setup.API_KEY)
    if (args.option == 'get'):
         response=image_profile.get_image_profile()
    elif (args.option == 'post'):
         image_profile.post_image_profile('aws-image-profile', 'Centos_API', '1379e0c7f41e875572339b934200', '2aaf79b789eee87557233fc948e31')
    else: 
        response = "INVALID options only get, post and delete are supported image options"
elif (args.resource == 'project'):
    projects = setup.Projects(setup.SERVER, setup.API_KEY)
    if (args.option == 'get'):
         response=projects.get_projects()
    elif (args.option == 'get_one'):
         response=projects.get_project_by_id(args.id)
    elif (args.option == 'post'):
         response=projects.post_create_project(args.name, admin_email, member_email, zoneid % (args.id))
    elif (args.option == 'delete'):
         response=projects.delete_project(args.id)
    else: 
        response = "INVALID options only get are supported project options"
elif (args.resource == 'account'):
    cloud_accounts = setup.CloudAccounts(setup.SERVER, setup.API_KEY)
    if (args.option == 'get'):
         response=cloud_accounts.get_cloud_accounts()
    elif (args.option == 'get_one'):
         response=cloud_accounts.get_cloud_account(args.id)
    elif (args.option == 'patch'):
         response=cloud_accounts.patch_account(args.name, 'aws', args.location, args.id) 
    else:
        response = "INVALID options only get, post and delete are supported image options"
elif (args.resource == 'region'):
    location = setup.Location(setup.SERVER, setup.API_KEY)
    if (args.option == 'get'):
        response = location.get_location_regions()
    else:
        response = "INVALID options only get, post and delete are supported region options"
elif (args.resource == 'zone'):
    location = setup.Location(setup.SERVER, setup.API_KEY)
    if (args.option == 'get'):
        response = location.get_location_zones()
    else: 
        response = "INVALID options only get, post and delete are supported zone options"
elif (args.resource == 'network'):
    network = setup.FabricNetwork(setup.SERVER, setup.API_KEY)
elif (args.resource == 'blueprint'):
    blueprint = setup.BluePrint(setup.SERVER, setup.API_KEY)
    if (args.option == 'get'):
        response = blueprint.get_blueprints('blueprints')
    elif (args.option == 'get_one'):
        response = blueprint.get_blueprint_by_id('blueprints', args.id, "NULL")
    elif (args.option == 'post'):
        response = blueprint.post_deploy_blueprint_(args.name, "test a deployment via API", args.id)
    elif (args.option == 'post_deploy'):
        response = blueprint.post_deploy_blueprint_yaml('MultiInputTier.yaml', args.id, args.name)
    elif (args.option == 'post_basic'):
        response = blueprint.post_deploy_blueprint_yaml('BasicUC1.yaml', args.id, args.name)
    else:
        response = "INVALID option.  Only get, post and delete are supported options"
elif (args.resource == 'deployment'):
    deployment = setup.Deployment(setup.SERVER, setup.API_KEY)
    if (args.option == 'get'):
        response = deployment.get_deployments()
    elif (args.option == 'get_one'):
        response = deployment.get_deployment_by_id(args.id)
    else:
        response = "INVALID option only get and get_one are supported options"
else:
    response = "Invalid resource - valid options are: flavor image blueprint network storage region"

print ("response is %s" % response.text)
print (len(response.text))
if ( len(response.text) != 0):
    print (json.dumps(response.json(), indent=2))

