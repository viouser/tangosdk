import requests
import doctest
import unittest
import yaml
import json
import logging

API_KEY = "b2c0ebea-ec54-4043-a869-f3e10411489d"
SERVER = 'api.mgmt.cloud.vmware.com'


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

class RestClient():
    """
     This is the client implementation based on "requests"
    """
    _URL_TEMPLATE_PREFIX = "https://%s/iaas/%s"
    
    _URL_BLUEPRINT_TEMPLATE_PREFIX = "https://%s/blueprint/api/%s"

    _URL_DEPLOYMENT_TEMPLATE_PREFIX = "https://%s/deployment/api/%s"

    def __init__(self, server, api_key):
        """ 
          set the remote server IP and API key
        """
        self._server = server
        self._api_key = api_key
        self._session = self._login('login')
        self._auth = self._do_login_bearer_token('login')
        
    def _generic_iaas_url(self, path):
        generic_path = path
        return self._URL_TEMPLATE_PREFIX % (self._server,generic_path)

    def _generic_blueprint_url(self, path):
        generic_path = path
        return self._URL_BLUEPRINT_TEMPLATE_PREFIX % (self._server, generic_path)

    def _generic_deployment_url(self, path):
        generic_path = path
        return self._URL_DEPLOYMENT_TEMPLATE_PREFIX % (self._server, generic_path)

    def _get_url(self, path, template_type):
        if (template_type == "IAAS"):
          url = self._generic_iaas_url(path)
        elif (template_type == "BLUEPRINT"):
          url = self._generic_blueprint_url (path)
        else:
          url = self._generic_deployment_url (path)
        return url

    def _login_payload (self):
        return "{" + "refreshToken" + ":" + str(self._api_key) + "}"

    def _append_id (self, path, id):
        temp_url = "%s/%s"
        return temp_url%(path, id)

    def _login(self, path):
        url = self._generic_iaas_url(path)
        payload = self._login_payload()
        session = requests.Session()
        session.post(url, data=payload)
        return session

    def _do_login_bearer_token(self, path):
        """
         Attributes:
             api_key: A string representing the API KEY.
             url: A string representing the ENDPOINT URL.
             payload: Description of what the DATA returns in response to a request.
             headers: Represent the meta-data associated with the API request and response.
        """
        url = self._generic_iaas_url(path)
        payload = self._login_payload()
        headers = {'accept': "application/json",'Content-Type': "application/json",'Cache-Control': "no-cache"}
        r = self._session.post(url, data=payload, headers=headers)
        auth = "Bearer " + r.json()['token']
        return auth

    def do_get(self, path, template_type):
        url = self._get_url(path, template_type)
        headers = {
          'Authorization': self._auth,
          'accept': "application/json",
          'Content-Type': "application/json",
          'Cache-Control': "no-cache"
        }
        response = self._session.get(url, headers=headers)
        return response

    def do_post(self, path, data, template_type):
        url = self._get_url(path, template_type)
        headers = {
          'Authorization': self._auth,
          'accept': "application/json",
          'Content-Type': "application/json",
          'Cache-Control': "no-cache"
        }
        response = self._session.post(url, data, headers=headers)
        return response

    def do_delete(self, path, template_type):
        url = self._get_url(path, template_type)
        headers = {
          'Authorization': self._auth,
          'accept': "application/json",
          'Content-Type': "application/json",
          'Cache-Control': "no-cache"
        }
        response = self._session.delete(url, headers=headers)
        return response

    def do_patch(self, path, data, template_type):
        url = self._get_url(path, template_type)
        headers = {
          'Authorization': self._auth,
          'accept': "application/json",
          'Cache-Control': "no-cache"
        }
        response = self._session.patch(url, data, headers=headers)
        return response

 
class FabricFlavors():
    """
    Attributes:
        flavor_name: A string representing flavor size i.e (SMALL, MEDIUM, LARGE)
        region: A string representing the availability zone i.e (AWS, AZURE)
        value: A string representing the value i.e (t2.nano, t2.large)
    """
    def __init__(self, server, api_key):
       self.rest_client = RestClient(server, api_key)
 
    def get_fabric_flavors(self):            
        response = self.rest_client.do_get('flavors', 'IAAS')
        logger.debug (' Fabric Flavor: %s',json.dumps(response.json(), indent=2))
        return response

class FabricImages():
    """ 
       Get all images defined in Fabric Images.
    """

    def __init__(self, server, api_key):
        self.rest_client = RestClient(server, api_key)

    def get_fabric_images(self):            
        response = self.rest_client.do_get('fabric-images', 'IAAS')
        logger.debug (' Fabric Image: %s',json.dumps(response.json(), indent=2))
        return response

class ImageProfile():
    """ 
       Get all image profiles.
    """

    def __init__(self, server, api_key):
        self.rest_client = RestClient(server, api_key)

    def get_image_profile(self):            
        response = self.rest_client.do_get('image-profiles', 'IAAS')
        logger.debug (' Image Profile: %s',json.dumps(response.json(), indent=2))
        return response
    
    _PAYLOAD_ = "{ name: %s, imageMapping: {%s: {id: %s}}, regionId: %s  }"

    def post_image_profile(self, name, ImageName, value, regionId):
        """ 
        Attributes:  
            name: A string representing flavor profile name
            value: A string representing a flavor profile value(t2.small)
            regionId: A string representing flavor profile regionId
        """
        payload = self._PAYLOAD_ % (name, ImageName, value, regionId)
        response = self.rest_client.do_post('image-profiles', payload, "IAAS")
        logger.debug (' Image Profil POSTe: %s',json.dumps(response.json(), indent=2))
        return response

class CloudAccounts():
    """ 
       Get All Cloud Accounts.
    """

    def __init__(self, server, api_key):
        self.rest_client = RestClient(server, api_key)

    def get_cloud_accounts(self):            
        response = self.rest_client.do_get('cloud-accounts', 'IAAS')
        logger.debug (' Cloud Accounts: %s',json.dumps(response.json(), indent=2))
        return response 

    def get_cloud_account(self, accountid):
        url = "cloud-accounts" + "/" + accountid
        response = self.rest_client.do_get(url, 'IAAS')
        logger.debug (' Account Detail: %s',json.dumps(response.json(), indent=2))
        return response

    def delete_cloud_account(self, accountid):
        url = "cloud-accounts" + "/" + accountid
        response = self.rest_client.do_delete(url, "IAAS")
        logger.debug (' Deleted Account: %s',json.dumps(response.json(), indent=2))
        return response

    _PAYLOAD_ = "{ name: %s, cloudAccountType: %s, privateKeyId: %s, privateKey: %s, cloudAccountProperties: %s, regionIds: [ %s ], createDefaultZones: false }"

    def post_create_accounts(self, accountname, accounttype, accountkeyid, accountkey, accountproperty, accountregion):
        """
        Attributes:
            accountname: A string representing account name
            accounttype: type of account. options are aws, azure, vsphere
            accountkeyid: 16 character privatekeyid
            privatekey: privatekey
            accountprperty: cloudAccountProperties example Azure: azureTenantId, userLinkvSphere: hostName, acceptSelfSignedCertificate, linkedCloudAccountLink, dcId
            regionIds: example us-east-1 or ap-northest-1
        """
        payload = self._PAYLOAD_ % (accountname, accounttype, accountkeyid, accountkey, accountproperty, accountregion)
        response = self.rest_client.do_post('cloud-accounts', payload, 'IAAS')
        logger.debug (' Create account response: %s',json.dumps(response.json(), indent=2))
        return response

    def patch_account(self, accountname, accounttype, accountkeyid, accountkey, regionid, accountid, accountproperty ):
        """
        Attributes:
            accountname: A string represent account name
            accounttype: type of account. options are aws, azure, vsphere
            accountkeyid: 16 character privatekeyid
            privatekey: privatekey
            accountprperty: cloudAccountProperties example Azure: azureTenantId, userLinkvSphere: hostName, acceptSelfSignedCertificate, linkedCloudAccountLink, dcId
            regionIds: example "us-east-1" or '"us-east-1", "us-east-2", "us-west-1"'
            accountid: UUID of the account

        """
        url = "cloud-accounts" + "/" + accountid
        payload = self._PAYLOAD_ % (accountname, accounttype, accountkeyid, accountkey, accountproperty, regionid)
        response = self.rest_client.do_patch(url, payload, 'IAAS')
        logger.debug (' Patch Account Response: %s',json.dumps(response.json(), indent=2))
        return response

        
class Projects():
    """ 
       Get all Projects.
    """
    _PAYLOAD_ = '{ name: "%s", description: "API Generated Project", administrators: [ %s ], members: [ %s ], zoneAssignmentConfigurations: [  %s  ]  }'

    def __init__(self, server, api_key):
        self.rest_client = RestClient(server, api_key)

    def get_projects(self):            
        response = self.rest_client.do_get('projects', 'IAAS')
        logger.debug (' Get Project Response: %s',json.dumps(response.json(), indent=2))
        return response

    def get_project_by_id(self, projectid):
        url = "projects" + "/" + projectid
        response = self.rest_client.do_get(url, 'IAAS')
        logger.debug (' Get Project by ID response: %s',json.dumps(response.json(), indent=2))
        return response

    def post_create_project(self, name, email_admin, email_member, zone):
        payload = self._PAYLOAD_%(name, email_admin, email_member, zone)
        response = self.rest_client.do_post('projects', payload, 'IAAS')
        logger.debug (' Create Project ID Response : %s',json.dumps(response.json(), indent=2))
        return response

    def delete_project (self, projectid):
        url = "projects" + "/" + projectid
        response = self.rest_client.do_delete(url, 'IAAS')
        logger.debug (' Delete Project Response: %s',json.dumps(response.json(), indent=2))
        return response
 
class Location():
    """ 
       Get all location zones / regions.
    """

    def __init__(self, server, api_key):
        self.rest_client = RestClient(server, api_key)

    def get_location_regions(self):            
        response = self.rest_client.do_get('regions', 'IAAS')
        logger.debug (' Get Region Response: %s',json.dumps(response.json(), indent=2))
        return response 
    
    def get_location_zones(self):            
        response = self.rest_client.do_get('zones', 'IAAS')
        logger.debug (' Get zone response: %s',json.dumps(response.json(), indent=2))
        return response

class FlavorProfile():
    """ 
       Get All Flavor Profiles.
    """

    def __init__(self, server, api_key):
        self.rest_client = RestClient(server, api_key)

    def get_flavor_profiles(self):            
        response = self.rest_client.do_get('flavor-profiles', 'IAAS')
        logger.debug (' Get flavor Profile Response: %s',json.dumps(response.json(), indent=2))
        return response


    def create_flavor_mapping(self, **kwargs):
        number = len(kwargs)
        _path_ = "%s : {name: %s}"
        final = ""
        i = 1
        for key, value in kwargs.iteritems():
            setattr(self, key, value)
            result = _path_ % (key, value)
            if i < number:
                seperator = ","
            else:
                seperator = ""
            i = i + 1
            final = final + result + seperator
        return final

    _PAYLOAD_ = "{name:%s, flavorMapping: {%s}, regionId: %s  }"

    def post_flavor_profile(self, name, flavorMap, regionId):
        """ 
        Attributes:
            name: A string representing flavor profile name
            image_name: A string representing flavor profile image_name
            value: A string representing a flavor profile value(t2.small)
            regionId: A string representing flavor profile regionId
        """

        payload = self._PAYLOAD_ % (name, flavorMap, regionId)
        response = self.rest_client.do_post('flavor-profiles', payload, "IAAS")
        logger.debug (' Flavor Profile update response: %s',json.dumps(response.json(), indent=2))
        return response

class FabricNetwork():
    """ 
       Get All Fabric Network.
    """

    def __init__(self, server, api_key):
        self.rest_client = RestClient(server, api_key)

    def get_fabric_network(self):            
        response = self.rest_client.do_get('fabric-networks', 'IAAS')
        logger.debug (' Network Fabric Response %s',json.dumps(response.json(), indent=2))
        return response

class FabricStorageAccount():
    """ 
       Get All Fabric Storage Accounts.
    """

    def __init__(self, server, api_key):
        self.rest_client = RestClient(server, api_key)

    def get_fabric_storage_account(self):            
        response = self.rest_client.do_get('fabric-storage-accounts', 'IAAS')
        logger.debug (' Fabric Storage Account Response: %s',json.dumps(response.json(), indent=2))
        return response  

class StorageProfile():
    """ 
       Get All Storage Profile.
    """

    def __init__(self, server, api_key):
        self.rest_client = RestClient(server, api_key)

    def get_storage_profiles(self):            
        response = self.rest_client.do_get('storage-profiles', 'IAAS')
        logger.debug (' Get Storage Profile Response: %s',json.dumps(response.json(), indent=2))
        return response 
   
    def get_storage_profiles_azure(self):            
        response = self.rest_client.do_get('storage-profiles-azure', 'IAAS')
        logger.debug (' Get Storage Profile Azure Image Response: %s',json.dumps(response.json(), indent=2))
        return response
   
    def get_storage_profiles_vsphere(self):            
        response = self.rest_client.do_get('storage-profiles-vsphere', 'IAAS')
        logger.debug (' Get Storage Profiles vSPhere Response: %s',json.dumps(response.json(), indent=2))
        return response  

class Deployment():
    """ 
       Get All deployment Profile.
    """
    def __init__(self, server, api_key):
        self.rest_client = RestClient(server, api_key)

    def get_deployments(self):
        response = self.rest_client.do_get('deployments', 'DEPLOYMENT')
        logger.debug (' Get Deployment Response: %s',json.dumps(response.json(), indent=2))
        return response

    def get_deployment_by_id(self, id):
        temp_url = self.rest_client._append_id('deployments', id) 
        response = self.rest_client.do_get(temp_url, 'DEPLOYMENT')
        logger.debug (' Get Deployment Details Response: %s',json.dumps(response.json(), indent=2))
        return response

class BluePrint():
    """ 
       Get available blueprints
    """
    def __init__(self, server, api_key):
        self.rest_client = RestClient(server, api_key)

    def get_blueprints(self, blueprint_type):
        response = self.rest_client.do_get(blueprint_type, 'BLUEPRINT')
        logger.debug (' Get Blueprint Response: %s',json.dumps(response.json(), indent=2))
        return response

    """ 
      two scenarios {blueprint_type}/{id} and {blueprint_type}/{id}/other action
      example: 
          blueprint-requests/{requestID}
          or
          blueprint-requests/{requestID}/execution
      set other to NULL to decide if temp url should have 2 or 3 strings
    """ 
    def get_blueprint_by_id(self, blueprint_type, id, other):
        if (other == "NULL"):
          temp_url = "%s/%s"
          blueprint_url = temp_url % (blueprint_type, id)
        else:
          temp_url = "%s/%s/%s"
          blueprint_url = temp_url % (blueprint_type, id, other)
        response = self.rest_client.do_get(blueprint_url, 'BLUEPRINT')
        logger.debug (' Get blueprint Details Response: %s',json.dumps(response.json(), indent=2))
        return response

    _PAYLOAD_ = '{ deploymentName: "%s", reason: "%s", blueprintId: "%s", inputs:{count:1}}'

    def post_deploy_blueprint_(self, deployment_name, reason, blueprint_id ):
        """
        Attributes:
            deployment_name: A string representing Deployment Name
            reason: A string representing reason of change
            blueprint_id: A string representing Blueprint Id
        """
        payload = self._PAYLOAD_ % (deployment_name, reason, blueprint_id)
        print (payload)
        response = self.rest_client.do_post('blueprint-requests', payload, "BLUEPRINT")
        logger.debug (' Deploy Blueprint Response: %s',json.dumps(response.json(), indent=2))
        return response

    _PAYLOAD_YAML = '{ name: "%s", projectId: "%s", description: "API-TEST A basic single-VM blueprint",  content: "%s", validate: False}'
    
    def post_deploy_blueprint_yaml(self, file, projectid, name):
        """
        Attributes:
            deployment_name: A string representing Deployment Name
            reason: A string representing reason of change
            blueprint_id: A string representing Blueprint Id
        """
        yaml_file = yaml.load(open(file))
        payload = self._PAYLOAD_YAML % (name, projectid, yaml_file)
        logger.debug (' Yaml Payload is: %s',payload)
        response = self.rest_client.do_post('blueprints', payload, "BLUEPRINT")
        logger.debug (' Deploy YAML File Response: %s',json.dumps(response.json(), indent=2))
        return response
   
class MyTest(unittest.TestCase):
   def test_fabric(self):
       '''
           - test that the response status code == 200 and placeholder for other tests
       '''
       flavor = FabricFlavors(SERVER, API_KEY)
       self.assertEqual(flavor.get_fabric_flavors().status_code, 200)

   def test_image(self):
       '''
           - test that the response status code == 200 and placeholder for other tests
       '''
       fabric_images = FabricImages(SERVER, API_KEY)
       self.assertEqual(fabric_images.get_fabric_images().status_code, 200)

   def test_image_profile(self):
       '''
           - test that the response status code == 200 and placeholder for other tests
       '''
       image_profile = ImageProfile(SERVER, API_KEY)
       self.assertEqual(image_profile.get_image_profile().status_code, 200)
       self.assertEqual(image_profile.post_image_profile('aws-image-profile', 'Centos_API', '1379e0c7f41e875572339b934200', '2aaf79b789eee87557233fc948e31').status_code, 200)

   def test_cloud_accounts(self):
       '''
           - test that the response status code == 200 and placeholder for other tests
       '''
       account_id = 'AKIAIMA3O7MUNDDE3NBQ'
       account_key= 'WcBTrOvJxeX8NffQOs3ZwQG3WxyAupsyIbs4cr9h'
       accountproperty = "{}"
       accountregion = '"us-east-1", "us-east-2", "us-west-1", "us-west-2"' 
       cloud_accounts = CloudAccounts(SERVER, API_KEY)
       tango_accountid = 'b7e400bc8699fe75-7f703c5265a63d87-3829d4006d24e635b2a45b88140610340cafb24f17e0a91d638a0e27e50606e0-34a91fd7f429527557233fc772ac1'
       self.assertEqual(cloud_accounts.get_cloud_accounts().status_code, 200)
#      create account seem to work only once.
#       self.assertEqual(cloud_accounts.post_create_accounts('xgao-TMM2', 'aws', account_id, account_key, accountproperty, accountregion).status_code, 200)
       self.assertEqual(cloud_accounts.get_cloud_account(tango_accountid).status_code, 200)
       self.assertEqual(cloud_accounts.patch_account('Xgao-TMM','aws', account_id, account_key, accountregion, tango_accountid, accountproperty).status_code, 200)

   def test_projects(self):
       '''
           - test that the response status code == 200 and placeholder for other tests
       '''
       admin_email = '{ "email": xiaog@vmware.com }'
       member_email = '{ "email": hmourad@vmware.com }'
       zoneid = '{ zoneId: deb311c914a480755735c23842ab8 }'
       projects = Projects(SERVER, API_KEY)
       self.assertEqual(projects.get_projects().status_code, 200)
       self.assertEqual(projects.post_create_project('API_Test_Generated', admin_email, member_email, zoneid).status_code, 200)

   def test_flavor_profile(self):
       '''
           - test that the response status code == 200 and placeholder for other tests
       '''   
       flavor_profile = FlavorProfile(SERVER, API_KEY)
       my_flavor_map = flavor_profile.create_flavor_mapping(small="t2.small", medium="t2.medium", large="t2.large")
       response = flavor_profile.post_flavor_profile('test', my_flavor_map, '2aaf79b789eee87557233fc948e31')
       self.assertEqual(response.status_code, 200)
       self.assertEqual(flavor_profile.get_flavor_profiles().status_code, 200)

   def test_region(self):
       '''
           - test that the response status code == 200 and placeholder for other tests
       '''
       location = Location(SERVER, API_KEY)
       self.assertEqual(location.get_location_regions().status_code, 200)
       self.assertEqual(location.get_location_zones().status_code, 200)

   def test_blueprint(self):
       '''
           - test that the response status code == 200 and placeholder for other tests
       '''
       blueprint = BluePrint(SERVER, API_KEY)
       self.assertEqual(blueprint.get_blueprints('blueprints').status_code, 200)
       self.assertEqual(blueprint.get_blueprints('blueprint-requests').status_code, 200)
       self.assertEqual(blueprint.post_deploy_blueprint_('API Test', 'test a deployment via API', 'b99faa206b93907557367e4bb9288').status_code, 200)
       self.assertEqual(blueprint.post_deploy_blueprint_yaml('BasicUC1.yaml', '02371a88-2b5c-413a-948d-902bf9ac8c31', 'Basic API Loaded').status_code,201)
       self.assertEqual(blueprint.post_deploy_blueprint_yaml('MultiInputTier.yaml', '02371a88-2b5c-413a-948d-902bf9ac8c31', 'Multi Tier API Loaded').status_code, 201)



if __name__ == '__main__':
    doctest.testmod()
    unittest.main()
